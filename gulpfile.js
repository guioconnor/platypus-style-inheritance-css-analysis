var gulp = require('gulp'),
    postcss = require('gulp-postcss'),
    path = require('path'),
    minifyCss = require('gulp-minify-css'),
    less = require('gulp-less'),
    processors = [
        require('postcss-mixins'),
        require('postcss-simple-vars'),
        require('postcss-nested'),
        function(css) {
            // sans-serif fallback
            css.eachDecl('font-family', function(decl) {
                decl.value = decl.value + ', sans-serif';
            });
        },
        require("css-mqpacker")(),
        require('autoprefixer')({ browsers: ['last 2 versions', '> 2%'] })
    ];

// compile CSS
gulp.task('css', function() {
  return gulp.src('src/css/styles.css')
    .pipe(postcss(processors))
    .pipe(minifyCss())
    .pipe(gulp.dest('dist/styles/'));
});

gulp.task('less', function () {
  return gulp.src('src/less/**/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./src/css'));
});

gulp.task('watch', function(){
    gulp.watch('src/css/**/*.css', ['css']);
    gulp.watch('src/less/**/*.less', ['less']);
});

gulp.task('default', ['watch']);