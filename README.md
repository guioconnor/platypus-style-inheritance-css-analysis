# README #

### What is this repository for? ###

This project uses a less file with a heavily unoptimal mixin inheritance style that generates very repetitive CSS. It then minifies it to allow for analysis on the size and shape of the generated stylesheet.

The most important aspects to look for on the generated result are

1. File size
2. Cascade use
3. Repetition/regrouping of css rules

### How do I get set up? ###

* Clone the repo
* cd into the repo's directory
* npm install
* gulp